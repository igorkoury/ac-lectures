package org.academiadecodigo.porto;

public class Player {
    //Property
    private String name;

    //Constructor
    public Player(String name) {
        this.name = name;
    }

    //Getter
    public String getName() {
        return this.name;
    }

    //Method
    public int pickNumber(int min, int max) {
        int range = (max - min) + 1;
        return (int) (Math.random()*min); // returns an integer between 0 and 10
    }
}
