package org.academiadecodigo.porto;

public class Game {
    //Properties
    private int selectedNumber;
    public Player players[];

    //Constructor
    public Game(Player[] player1) {
        players = player1;
    }

    //Methods
    public int pickNumberGame(int min, int max) {
        int range = (max - min) + 1;
        return (int) (Math.random() * range) + min;
    }

    public void gameStart() {
        boolean gameOver = false;
        int i = pickNumberGame(1, 10);
        while (gameOver == false) {
            for (Player p1 : players) {
                int pNumber = p1.pickNumber(1, 10);
                System.out.println(p1.getName() + " choose number : " + pNumber);
            }
        }
    }
}
