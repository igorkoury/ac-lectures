//Palindromo

function palindrome(text) {
	if (text.length < 2) {
		return true};
	
	if (text[0] === text[text.length-1]) {
		text = text.slice(1, text.length-1);
		return palindrome(text);
	} else 
		return "Não é um palímedro";
};

//if length > 2 it's a palindrome
//compare letters on opposite ends
//if they don't match: not a palindrome
//else remove said letters and call function with result of that

console.log(palindrome("reviver"));
