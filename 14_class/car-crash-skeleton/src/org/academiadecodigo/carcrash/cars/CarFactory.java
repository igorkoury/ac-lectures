package org.academiadecodigo.carcrash.cars;

import org.academiadecodigo.carcrash.field.Field;
import org.academiadecodigo.carcrash.field.Position;

public class CarFactory {

    private CarType carType;
    private Car car;
    private Position pos;

    public static Car getNewCar() {

        int col = (int) (Math.random() * Field.getWidth());
        int row = (int) (Math.random() * Field.getHeight());
        int type = (int) (Math.random() * 2);

        Position pos = new Position(col, row);

        if (type % 2 == 0) {
            return new Fiat(pos);
        } else {
            return new Mustang(pos);
        }

    }

}
