package org.academiadecodigo.carcrash.field;

public class Position {

    private int col;
    private int row;

    public Position(int col, int row) {
        this.col = col;
        this.row = row;
    }

    public int getCol() {
        return this.col;
    }

    public int getRow() {
        return this.row;
    }

    public void changeDirection() {
        double probapility = ((double) Math.random());
        if (probapility < 0.05) {
            changeDirection();
        }
        int direction = (int) (Math.random() * 4);
        switch (direction) {
            case 0:
                if (getCol() == Field.getWidth() - 1) {
                    changeDirection();
                } else {
                    col++; // move right
                }
                break;
            case 1:
                if (getCol() == 0) {
                    changeDirection();
                } else {
                    col--; // move left
                }
                break;
            case 2:
                if (getRow() == Field.getHeight() - 1) {
                    changeDirection();
                } else {
                    row++; // move dow
                }
                break;
            case 3:
                if (getRow() == 0) {
                    changeDirection();
                } else {
                    row--; // move up
                }
                break;

        }
    }
}
