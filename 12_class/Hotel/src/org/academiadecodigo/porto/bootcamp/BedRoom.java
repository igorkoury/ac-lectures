package org.academiadecodigo.porto.bootcamp;

public class BedRoom {

    private int roomNumber;
    private Client client;
    private boolean busy;

    public BedRoom(int roomNumber) {
        this.roomNumber = roomNumber;
        this.busy = false;
    }

    public void setClient(Client client){
        this.client = client;
    }

    public boolean isBusy() {
        return busy;
    }

    public void setBusy(boolean busy) {
        this.busy = busy;
    }
}


