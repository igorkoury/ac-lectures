package org.academiadecodigo.porto.bootcamp;

public class Hotel {

    private String name;
    BedRoom[] bedrooms;

    public Hotel(String name) {
        bedrooms = new BedRoom[10];
        buildRooms();
    }

    private void buildRooms() {
        for(int i = 0; i< bedrooms.length;i++){
            bedrooms[i] = new BedRoom(i);
        }
    }

    public void checkIn(String client, int roomNumber) {
        if(this.bedrooms[roomNumber].isBusy()) {
            System.out.println("Room is busy");
            return;
        }
        this.bedrooms[roomNumber].setBusy(true);
        this.bedrooms[roomNumber].setClient(new Client(client));
        System.out.println(client + ", made a check-in on bedroom ");
    }


    public void checkOut(int roomNumber) {
        //e se o quarto ja tiver livre?
        this.bedrooms[roomNumber].setBusy(false);
        this.bedrooms[roomNumber].setClient(null);
        System.out.println(", made a check-out on bedroom ");
    }

}
