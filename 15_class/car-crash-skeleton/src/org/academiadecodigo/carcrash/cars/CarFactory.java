package org.academiadecodigo.carcrash.cars;

import org.academiadecodigo.carcrash.field.Field;
import org.academiadecodigo.carcrash.field.Position;

import java.util.Random;

public class CarFactory {

    public static Car getNewCar() {

        Random rand = new Random();
        int col = rand.nextInt(Field.getWidth()); //0-99
        int row = rand.nextInt(Field.getHeight()); //0-24
        Position pos = new Position(col, row);

        if (rand.nextInt() % 2 == 0) {
            return new Fiat(pos);
        } else {
            return new Mustang(pos);
        }
    }
}
