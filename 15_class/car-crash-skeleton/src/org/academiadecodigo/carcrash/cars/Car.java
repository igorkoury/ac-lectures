package org.academiadecodigo.carcrash.cars;

import org.academiadecodigo.carcrash.field.Field;
import org.academiadecodigo.carcrash.field.Position;

abstract public class Car {

    protected CarType carType;

    /**
     * The position of the car on the grid
     */
    private Position pos;
    protected int step;

    private boolean crashed = false;

    public Car(Position pos) {
        this.pos = pos;
    }

    public Position getPos() {
        return pos;
    }

    public boolean isCrashed() {
        return crashed;
    }

    public void setCrashed(boolean crashed) {
        this.crashed = crashed;
    }

    public void moveDown() {
        if (crashed) {
            return; // verifica se está ou não batido
        }
        if (pos.getRow() + step > Field.getHeight() - 1) {
            moveUp(); // move para cima se ele já estiver no limite para baixo
        } else {
            this.pos.setRow(this.pos.getRow() + step);
        }
    }

    public void moveUp() {
        if (crashed) {
            return;
        }
        if (pos.getRow() - step < 0) {
            moveDown();
        } else {
            this.pos.setRow(this.pos.getRow() - step);
        }
    }

    public void moveRight() {
        if (crashed) {
            return;
        }
        if (pos.getCol() + step > Field.getWidth() - 1) {
            moveLeft();
        } else {
            this.pos.setCol(this.pos.getCol() + step);
        }
    }

    public void moveLeft() {
        if (crashed) {
            return;
        }
        if (pos.getCol() - step < 0) {
            moveRight();
        } else {
            this.pos.setCol(this.pos.getCol() - step);
        }
    }

    @Override
    public String toString() {
        return carType.name().charAt(0) + ""; // colocar aspas transforma em string
    }
}
