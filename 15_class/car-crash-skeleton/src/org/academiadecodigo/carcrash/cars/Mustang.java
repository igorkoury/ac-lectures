package org.academiadecodigo.carcrash.cars;

import org.academiadecodigo.carcrash.field.Position;

public class Mustang extends Car {

    public Mustang(Position pos) {
        super(pos);
        step = 2;
        carType = CarType.MUSTANG;
    }
}
