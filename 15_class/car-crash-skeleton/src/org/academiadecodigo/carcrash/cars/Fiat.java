package org.academiadecodigo.carcrash.cars;

import org.academiadecodigo.carcrash.field.Position;

public class Fiat extends Car{

    public Fiat(Position pos) {
        super(pos);
        step = 1;
        carType = CarType.FIAT;
    }


}
