package org.academiadecodigo.carcrash;

import org.academiadecodigo.carcrash.cars.Car;
import org.academiadecodigo.carcrash.cars.CarFactory;
import org.academiadecodigo.carcrash.field.Field;

import java.util.Random;

public class Game {

    public static final int MANUFACTURED_CARS = 20;

    /**
     * Container of Cars
     */
    private Car[] cars;

    /**
     * Animation delay
     */
    private int delay;

    public Game(int cols, int rows, int delay) {

        Field.init(cols, rows);
        this.delay = delay;

    }

    /**
     * Creates a bunch of cars and randomly puts them in the field
     */
    public void init() {

        cars = new Car[MANUFACTURED_CARS];
        for (int i = 0; i < cars.length; i++) {
            cars[i] = CarFactory.getNewCar();
        }

        Field.draw(cars);

    }

    /**
     * Starts the animation
     *
     * @throws InterruptedException
     */
    public void start() throws InterruptedException {

        while (true) {

            // Pause for a while
            Thread.sleep(delay);

            checkCollision();

            // Move all cars
            moveAllCars();

            // Update screen
            Field.draw(cars);

        }

    }

    private void moveAllCars() {

        Random rand = new Random();
        for (Car car : cars) {
            int direction = rand.nextInt(4);
            switch (direction) {
                case 0:
                    car.moveUp();
                    break;
                case 1:
                    car.moveDown();
                    break;
                case 2:
                    car.moveRight();
                    break;
                case 3:
                    car.moveLeft();
                    break;
                default:
                    break;
            }
        }

    }

    public void checkCollision() {
        for (int i = 0; i < cars.length - 1; i++) {
            //ultima pos...?
            Car c = cars[i];
            for (int j = i + 1; j < cars.length; j++) {
                Car c1 = cars[j];
                if (c.getPos().equals(c1.getPos())) {
                    cars[i].setCrashed(true);
                    cars[j].setCrashed(true);
                }


            }
        }
    }


}
