
WORKING AT <A/C_> AND THEN GOING HOME
=====================================

**At <A/C_> when we announce a new challenge**

mkdir folder-name
cd folder-name
git init
vim .gitignore // put the output of a site like .gitignore here!
// write beautiful java code
git add .
git commit -m "write a descriptive message here"


**Time to make a remote "bridge" to use at home**

ssh firstnamelastname@porto.codecadets.academiadecodigo.org
mkdir project-name.git
cd project-name.git
git init --bare
exit

**Still at <A/C_> in the directory where I was working initially**

git remote add origin firstnamelastname@porto.codecadets.academiadecodigo.org:project-name.git
git push origin master

**Back at your home, first time grabbing the code**

git clone firstnamelastname@porto.codecadets.academiadecodigo.org:project-name.git
// that's it! your work is now on your home computer! with the .gitignore file and everything
// do some more work if you want
git add .
git commit -m "write another descriptive message here!"
git push origin master

**Next day, at <A/C_> and you want that sweet updated code**
//go into the folder you created before for the project
git pull origin master
// that's it!!! then just write code, git add, git commit, git push origin master etc etc etc...

**Again, back at home**

git pull origin master
// that's it!!



WORKING WITH THE REPO ON GITLAB
============================================

**You don't have this repo on your computer yet? Then do this!**

git clone https://gitlab.com/ac-bootcamp-repos/68_bootcamp/class-repo

**Now, time to put your files on the repo**
//go into either the summarizers or homeworks folder and put your file there
git add .
git commit -m "descriptive message about what you did"
git push origin master

**I already did clone and did the checkout to master before**

git pull origin master 
// this will make sure you have the latest changes
// now follow the same steps in the "time to put your files on the repo" section!

WORKING WITH THE CLASS REPO ON GITLAB
=====================================

**I don't have this repo on my computer yet!**

git clone https://gitlab.com/ac-bootcamp-repos/68_bootcamp/class-repo
// that's it!

**I've already done clone before for this repo, its already on my computer**
// go inside the class-repo folder
git pull origin master
// that's it!!


Very important... NEVER WORK INSIDE THE CLASS-REPO FOLDER! :) and NEVER on the remote machine!