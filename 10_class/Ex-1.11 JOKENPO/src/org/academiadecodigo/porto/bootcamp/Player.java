package org.academiadecodigo.porto.bootcamp;

import java.util.Random;

public class Player {

    private String name;
    private Jokenpo play;

    public Player(String name) {
        this.play = Jokenpo.randomize();
        this.name = name;
    }

    public enum Jokenpo {
        ROCK,
        PAPER,
        SCISSOR;

        public static Jokenpo randomize() {
            Jokenpo[] play = Jokenpo.values();
            Random generator = new Random();
            return play[generator.nextInt(play.length)];
        }
    }

    public String getName() {
        return name;
    }

    public Jokenpo getPlay() {
        return play;
    }


}
