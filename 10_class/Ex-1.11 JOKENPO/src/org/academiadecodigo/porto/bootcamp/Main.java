package org.academiadecodigo.porto.bootcamp;

public class Main {

    public static void main(String[] args) {

        Player p1 = new Player("Raquel");
        Player p2 = new Player("Machine");

        System.out.println("----------JOKENPO----------");
        System.out.println(p1.getName() + " choose: " + p1.getPlay());
        System.out.println(p2.getName() + " choose: " + p2.getPlay());
        System.out.println("---------------------------");

        switch (p1.getPlay()) {
            case ROCK:
                if (p2.getPlay() == Player.Jokenpo.ROCK) {
                    System.out.println("DRAW!!");
                    break;
                } else if (p2.getPlay() == Player.Jokenpo.PAPER) {
                    System.out.println("YOU LOSE!!");
                    break;
                } else if (p2.getPlay() == Player.Jokenpo.SCISSOR) {
                    System.out.println("YOU WIN!!");
                    break;
                }
            case PAPER:
                if (p2.getPlay() == Player.Jokenpo.PAPER) {
                    System.out.println("DRAW!!");
                    break;
                } else if (p2.getPlay() == Player.Jokenpo.ROCK) {
                    System.out.println("YOU WIN!!");
                    break;
                } else if (p2.getPlay() == Player.Jokenpo.SCISSOR) {
                    System.out.println("YOU LOSE!!");
                    break;
                }
            case SCISSOR:
                if (p2.getPlay() == Player.Jokenpo.PAPER) {
                    System.out.println("YOU WIN!!");
                    break;
                } else if (p2.getPlay() == Player.Jokenpo.ROCK) {
                    System.out.println("YOU LOSE!!");
                    break;
                } else if (p2.getPlay() == Player.Jokenpo.SCISSOR) {
                    System.out.println("DRAW!!");
                    break;
                }
        }

    }
}