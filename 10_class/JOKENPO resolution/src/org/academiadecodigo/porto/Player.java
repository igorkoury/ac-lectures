package org.academiadecodigo.porto;

public class Player {

    private String name;
    private int victories = 0;

    public Player(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getVictories() {
        return victories;
    }

    public void win() {
        this.victories++;
    }

    public GameHand chooseHand() {
        return Gamehand.values()[(int) (Math.random() * GameHand.values().length)];
        //[ROCK, PAPER, SCISSOR[2]
    }
}

