package org.academiadecodigo.porto.bootcamp;

public class Wallet {

    private int money = 0;

    public int getMoney() {
        return money;
    }

    public void add(int money) {
        this.money += money;
    }

    public int use(int money) {

        if(money > this.money) {
            int removedMoney = this.money;
            this.money = 0;
            return removedMoney;
        }

        this.money -= money;
        return money;
    }
}
