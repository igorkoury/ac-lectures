package org.academiadecodigo.porto.bootcamp;

public class PiggyBank {

    private int money;

    public int getMoney() {return money;}

    public int withdraw(int money)  {

        // exception case handling
        if (money > this.money) {
            int removedMoney = this.money;
            this.money = 0;
            return removedMoney;
        }

        // normal case
        this.money -= money;
        return money;
    }

    public void deposit(int money) {
        this.money +=money;
    }

}
