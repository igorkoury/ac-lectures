package org.academiadecodigo.porto.bootcamp;

public class Person {

    private String name;

    private PiggyBank piggyBank;
    private Wallet wallet;

    public Person(String name, PiggyBank piggyBank, Wallet wallet, int money) {

        this.name = name;
        this.piggyBank = piggyBank;
        this.wallet = wallet;

        wallet.add(money);
    }

    // return amount of money spend
    public int spend(int money) {return wallet.use(money);}

    public void fillWallet(int money) {piggyBank.withdraw(money);}

    public void saveMoney(int money) {piggyBank.deposit(wallet.use(money));}
}
